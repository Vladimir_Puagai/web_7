<!DOCTYPE html>

<html lang="ru">
<head>
	<meta name="yandex-verification" content="8652867351c14916" />
    <meta name="google-site-verification" content="_SDArifioMhP5wPO_negZVEkXYycCHND1v4JKXHQ-rU" />
    <title>Постройка дешевых зиккуратов в Краснодаре | Нер'зул корпорейшн - лучшие зиккураты для Плети</title>   
	<meta charset="utf-8">
    <link rel="shortcut icon" href="http://imagesait.ru/photos/aHR0cDovL2NzNi5waWthYnUucnUvaW1hZ2VzL3ByZXZpZXdzX2NvbW0vMjAxNS0wMV82LzE0MjI0MDEzMjcxNTY3LmpwZw==/zikkurat-varkraft-3.jpg" type="image/png">
    <meta name="description" content="Нер'зул корпорейшн. Зиккураты Краснодара. Дешевые зиккураты. Зиккураты для Плети. Постройка зиккуратов.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css" type="text/css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="./slick/slick.css">
	<link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
	<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
	<script src="./slick/slick.js" defer></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="script.js" defer></script> 
</head>

<body>
	<div id="background">
	
	<header>
        <h1>Нер'зул корпорейшн<br>Самые дешевые зиккураты Краснодара</h1>
    </header>
	
	<nav class="container mb-3 mt-2 bg-white">
		<div class="row">
			<div class="col-sm-12 col-md-4"><a href="#corporation" title="О корпорации">Корпорация</a></div>
            <div class="col-sm-12 col-md-4"><a href="#ziggurat" title="Виды зиккуратов">Зиккураты</a></div>
            <div class="col-sm-12 col-md-4"><a href="#corporation_addres" title="Переход к форме">Адресс корпорации</a></div>	
		</div>
	</nav>
	
	<div id="slides">
		<div class="single-slide">
			<div><img class="slide" src="./smile/1.png" alt=""></div>
			<div><img class="slide" src="./smile/2.png" alt=""></div>
			<div><img class="slide" src="./smile/3.png" alt=""></div>
			<div><img class="slide" src="./smile/4.png" alt=""></div>
			<div><img class="slide" src="./smile/5.png" alt=""></div>
			<div><img class="slide" src="./smile/6.png" alt=""></div>
			<div><img class="slide" src="./smile/7.png" alt=""></div>
			<div><img class="slide" src="./smile/8.png" alt=""></div>
			<div><img class="slide" src="./smile/9.png" alt=""></div>
			<div><img class="slide" src="./smile/10.jpeg" alt=""></div>
			<div><img class="slide" src="./smile/11.png" alt=""></div>
			<div><img class="slide" src="./smile/12.png" alt=""></div>
			<div><img class="slide" src="./smile/13.png" alt=""></div>
			<div><img class="slide" src="./smile/14.png" alt=""></div>
			<div><img class="slide" src="./smile/15.png" alt=""></div>
			<div><img class="slide" src="./smile/16.png" alt=""></div>
			<div><img class="slide" src="./smile/17.png" alt=""></div>
			<div><img class="slide" src="./smile/18.png" alt=""></div>
			<div><img class="slide" src="./smile/19.png" alt=""></div>
			<div><img class="slide" src="./smile/20.png" alt=""></div>
			<div><img class="slide" src="./smile/21.png" alt=""></div>
			<div><img class="slide" src="./smile/22.png" alt=""></div>
			<div><img class="slide" src="./smile/23.png" alt=""></div>
			<div><img class="slide" src="./smile/24.png" alt=""></div>
		</div>
	</div>
	
	<div id="Description" class="container mb-3 mt-2">
        <div class="row">
            <h1 id="corporation">Нер'зул корпорейшн</h1>
        </div>
        <div class="row">
            <div class="col">
				После победы Артаса над Иллиданом принц Лордерона освобождает душу Нер'зула. Освободившись от власти Легиона Нер'зул отходит от дел и открывает свою корпорацию по постройке зиккуратов.
            </div>
        </div>
    </div>
	
	<div id="ziggurat" class="container mb-3 mt-2">
        <div id="center1">
                <h1>Наши зиккураты</h1>
        </div>
		
		<div itemscope itemtype="http://schema.org/Product" class="product">
		    <div hidden itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                <span itemprop="ratingValue">100</span>
                из <span itemprop="bestRating">100</span>
                основано на <span itemprop="ratingCount">666</span> пользовательских оценках
            </div>
            <div hidden itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                <link hidden="" itemprop="url" href="/">
                <meta itemprop="price" content="100">
                <meta itemprop="priceCurrency" content="BTC">
                <meta itemprop="availability" hidden="" content="https://schema.org/InStock">
                <meta hidden="" itemprop="priceValidUntil" content="2024-04-04">
            </div>
            <meta itemprop="sku" content="0234578643012">
            <meta itemprop="mpn" content="4500001348">
            <div itemprop="brand" hidden="" itemtype="http://schema.org/Brand" itemscope="">
                <meta hidden itemprop="name" content="Нер'зул корпорейшн">
            </div>
            <div itemprop="name">
                <h2>Зиккурат</h2>
            </div>
            <div itemprop="description" class="row">
                <div class="mb-3 mt-2">
                    <img itemprop="image" src="http://www.rubattle.net/images/warcraft3/undead/buildings/ziggurat.gif" alt="Зиккурат">
                </div>
                <div class="col">
                    <span><em>Нет ничего лучше зиккурата в своем дворе.<br/>Дешевый и строится за считанные дни.<br/>Не требователен к золоту.</em></span>
                </div>
            </div>
            <div id="otziv1" class="row review-content" itemprop="review" itemscope itemtype="http://schema.org/Review">
                <div class="mb-3 mt-2">
                    <img src="poslushnik.jpg" alt="Послушник">
                </div>
                <div class="col person">
                    <div class="row name">
                        <span itemprop="author"><span>Послушник</span></span>
                    </div>
                    <div class="row city">
                        <span>Культ Проклятых</span>
                    </div>
                    <meta itemprop="datePublished" content="2011-03-25">3 год после Второй войны
                    <div class="row">
                        <div class="col-12">
                            Отзыв об <span itemprop="itemReviewed">зиккурате</span>
                        </div>
                    </div>
					 <div class="col row">
						<span itemprop="description" class="desc">
							<audio controls>
								<source src="Nuzhno-bolshe-zolota.mp3">
								<source src="Nuzhno-bolshe-zolota.ogg">
								<source src="Nuzhno-bolshe-zolota.wav">
							</audio>
						</span>
					</div>
                </div>
            </div>
        </div>
		
		<div itemscope itemtype="http://schema.org/Product" class="product">
		    <div hidden itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                <span itemprop="ratingValue">100</span>
                из <span itemprop="bestRating">100</span>
                основано на <span itemprop="ratingCount">666</span> пользовательских оценках
            </div>
            <div hidden itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                <link hidden="" itemprop="url" href="/">
                <meta itemprop="price" content="145">
                <meta itemprop="priceCurrency" content="BTC">
                <meta itemprop="availability" hidden="" content="https://schema.org/InStock">
                <meta hidden="" itemprop="priceValidUntil" content="2024-04-04">
            </div>
            <meta itemprop="sku" content="07635453012">
            <meta itemprop="mpn" content="45000015678">
            <div itemprop="brand" hidden="" itemtype="http://schema.org/Brand" itemscope="">
                <meta hidden itemprop="name" content="Нер'зул корпорейшн">
            </div>
            <div itemprop="name">
                <h2>Башня духов</h2>
            </div>
            <div itemprop="description" class="row">
                <div class="mb-3 mt-2">
                    <img itemprop="image" src="http://www.rubattle.net/images/warcraft3/undead/buildings/spirit_tower.gif" alt="Башня духов">
                </div>
                <div class="col">
                    <span><em>Более продвинутая версия зиккурата.<br/>Поможет в обороне огорода.<br/>Дешевле чем у нас не найти.<br/>Более требователен к золоту.</em></span>
                </div>
            </div>
            <div id="otziv2" class="row review-content" itemprop="review" itemscope itemtype="http://schema.org/Review">
                <div class="mb-3 mt-2">
                    <img src="poslushnik.jpg" alt="Другой послушник">
                </div>
                <div class="col person">
                    <div class="row name">
                        <span itemprop="author"><span>Другой послушник</span></span>
                    </div>
                    <div class="row city">
                        <span>Культ Проклятых</span>
                    </div>
                    <meta itemprop="datePublished" content="2011-03-25">Год до Третьей войны
                    <div class="row">
                        <div class="col-12">
                            Отзыв об <span itemprop="itemReviewed">зиккурате</span>
                        </div>
                    </div>
					 <div class="col row">
						<span itemprop="description" class="desc">
							<audio controls>
								<source src="jajdusluhit.mp3">
								<source src="jajdusluhit.ogg">
								<source src="jajdusluhit.wav">
							</audio>
							<br/>Построили быстро и дешево.
						</span>
					</div>
                </div>
            </div>
        </div>
	</div>
   
	<div id="center2">
	<div id="corporation_addres" class="container mb-3 mt-2" itemscope="" itemtype="https://schema.org/PostalAddress">
        <h1 itemprop="name">Адрес предприятия</h1>
        <p class="text text-center" itemprop="addressCountry">Плеть</p>
        <p></p>
        <p class="text text-center" itemprop="addressRegion">Цитадель Ледяной Короны</p>
        <p></p>
        <p></p>
        <p class="text text-center" itemprop="streetAddress">Ледяная корона<br>Ледяной Трон</p>
    </div>
	</div>	
	
	<footer>
    <small>За качество не отвечаем</small>
	</footer>
	
	</div>
  </body>
</html>

